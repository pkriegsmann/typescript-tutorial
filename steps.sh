#!/bin/bash

file=app.ts

if [ -z "$1" ]; then
    echo "No argument."
    exit 1
fi

if [ "$1" -eq 0 ]; then
    for i in *.ts; do
        rm $i
    done
    if [ -d build ]; then
        rm -r build
    fi

fi 

if [ "$1" -eq 1 ]; then
    echo -n "//Standard JavaScript
console.log('Hallo Welt!');" > $file
fi 

if [ "$1" -eq 2 ]; then
    echo -n "//text ist ein String
let text = 'Hallo Welt!';

//Zuweisung eines Integers ist nicht möglich
// text = 42;

console.log(text);" > $file
fi 

if [ "$1" -eq 3 ]; then
    echo -n "//interface ist ein Konstrukt, das ein Standard deklariert
interface Course {
    description: string,
    status: boolean
};

//Objekt muss dem Interface entsprechen
const cylab: Course = {
    description: 'Cylab',
    status: true
};" > $file
fi 

if [ "$1" -eq 4 ]; then
    echo -n "//members ist ein optionaler Parameter
interface Course {
    description: string,
    status: boolean,
    members?: number,
};

const cylab: Course = {
    description: 'Cylab',
    status: true
};" > $file
fi 

if [ "$1" -eq 5 ]; then
    echo -n "//Typangabe für Übergabeparameter und Rückgabetyp
const add = function (left: number, right: number): number {
    return left + right;
}

//Typ unbekannt, muss durch Abfragen überprüft werden
const funcA = function (value: unknown): void {
    if (typeof value === 'number') {
        //...
    }
    //...
}

//jeder Typ ist zulässig, Operationen werden ausgeführt, kann zu Laufzeitfehlern führen
const funcB = function (value: any): void {
    //...
}

//never - kein gültiger Funktionsabschluss
const funcC = function (): never {
    throw new Error();
}" > $file
fi 



if [ "$1" -eq 6 ]; then
    echo -n "//status ist ein Union
interface Course {
    description: string,
    status: 'open' | 'done' | 'in progress',
};

const cylab: Course = {
    description: 'Cylab',
    status: 'done',
};" > $file
fi 

if [ "$1" -eq 7 ]; then
    echo -n "// alias mit Union
type Status = 'open' | 'done' | 'in progress';

interface Course {
    description: string,
    status: Status,
};

const cylab: Course = {
    description: 'Cylab',
    status: 'done',
};" > $file
fi 

if [ "$1" -eq 8 ]; then
    echo -n "//Generics
interface Course<T> {
    description: string;
    data: T;
};

interface AssigneeData {
    assignee: string;
}

interface People {
    people: Array<string>;
}

const cylab: Course<AssigneeData> = {
    description: 'Cylab',
    data: {
        assignee: 'Hugo'
    }
};

const gD: Course<People> = {
    description: 'Game Design',
    data: {
        people: ['Max', 'Sarah']
    }
};" > $file
fi 

status=Status.ts
course=Course.ts

if [ "$1" -eq 9 ]; then
    echo -n "//Auslagern des Typs Status in eine extra Datei
type Status = 'open' | 'done' | 'in progress';

//nach außen sichtbar durch export
export { Status };" > $status

    echo -n "//Auslagern des Interfaces Course in eine extra Datei

//Importieren des Moduls Status
import { Status } from './Status';

interface Course {
    description: string,
    status: Status,
};
//Auslagern des Moduls Course in eine extra Datei
export { Course }" > $course 
    echo -n "//Importieren des Moduls Course
import { Course } from './Course';

const cylab: Course = {
    description: 'Cylab',
    status: 'done',
};" > $file
fi 

course_list=CourseList.ts

if [ "$1" -eq 10 ]; then
    echo -n "//Auslagern des Typs Status in eine extra Datei
type Status = 'open' | 'done' | 'in progress';

//nach außen sichtbar durch export
export { Status };" > $status

    echo -n "//Auslagern des Interfaces Course in eine extra Datei

//Importieren des Moduls Status
import { Status } from './Status';

interface Course {
    description: string,
    status: Status,
};
//Auslagern des Moduls Course in eine extra Datei
export { Course }" > $course 
    echo -n "import { CourseList } from './CourseList';

const courseList: CourseList = new CourseList();
courseList.add('cylab', 'open');
courseList.add('game design', 'done');" > $file

    echo -n "import { Course } from './Course';
import { Status } from './Status';

class CourseList {
    private courses: Course[];

    public constructor() {
        this.courses = []
    }

    public add( description: string, status: Status ) {
        const course: Course = {
            description,
            status
        }

        this.courses.push(course);
    }

}

export { CourseList };" > $course_list
fi 

if [ "$1" -eq 11 ]; then
    echo -n "//strukturelles Typsystem
type Status = 'open' | 'done' | 'in progress';

interface Course {
    description: string,
    status: Status,
};

interface NoCourse {
    description: string,
    status: Status,
};

const noCourse: NoCourse = {
    description: 'no Course',
    status: 'in progress',
};

function x(course: Course) {
    console.log(course)
}

x(noCourse);" > $file
fi 